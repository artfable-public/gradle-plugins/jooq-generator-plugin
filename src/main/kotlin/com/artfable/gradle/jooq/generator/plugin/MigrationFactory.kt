package com.artfable.gradle.jooq.generator.plugin

import liquibase.Contexts
import liquibase.LabelExpression
import liquibase.Liquibase
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.DirectoryResourceAccessor
import org.flywaydb.core.Flyway
import java.io.File

/**
 * @author artfable
 * 15/06/2023
 */
fun migration(tool: JooqGeneratorExtension.MigrationTool, sourcePath: String, projectDir: File, config: DBConnectionConfig): Migration {
    return when (tool) {
        JooqGeneratorExtension.MigrationTool.LIQUIBASE -> LiquibaseMigration(sourcePath, projectDir)
        JooqGeneratorExtension.MigrationTool.FLYWAY -> FlywayMigration(sourcePath, config)
    }
}

sealed interface Migration {
    suspend fun run(config: DBConnectionConfig, timeout: Long)
}

class LiquibaseMigration(
    private val changelog: String,
    private val output: File
) : Migration {
    override suspend fun run(config: DBConnectionConfig, timeout: Long) {
        val liquibase = Liquibase(
            changelog,
            DirectoryResourceAccessor(output),
            JdbcConnection(openDBConnectionWithRetry(config, timeout))
        )

        liquibase.update(Contexts(), LabelExpression())
    }
}

class FlywayMigration(
    private val dir: String,
    private val dbConfig: DBConnectionConfig
): Migration {
    override suspend fun run(config: DBConnectionConfig, timeout: Long) {
        Flyway.configure()
            .dataSource(dbConfig.jdbc, dbConfig.user, dbConfig.password)
            .connectRetriesInterval(1)
            .connectRetries((timeout / 1000).toInt())
            .locations(dir)
            .load()
            .migrate()
    }

}
