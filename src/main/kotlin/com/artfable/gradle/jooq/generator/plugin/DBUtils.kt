package com.artfable.gradle.jooq.generator.plugin

import kotlinx.coroutines.delay
import kotlinx.coroutines.withTimeout
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.*

/**
 * @author artfable
 * 16/06/2023
 */

private fun opeDBConnection(dbConnectionConfig: DBConnectionConfig): Connection {
    val properties: Properties = Properties()
    properties["user"] = dbConnectionConfig.user
    properties["password"] = dbConnectionConfig.password

    return DriverManager.getConnection(dbConnectionConfig.jdbc, properties)
}

suspend fun openDBConnectionWithRetry(dbConnectionConfig: DBConnectionConfig, timeout: Long): Connection =
    withTimeout(timeout) {
        openDBConnectionWithRetry(dbConnectionConfig)
    }

private suspend fun openDBConnectionWithRetry(dbConnectionConfig: DBConnectionConfig): Connection {
    return try {
        opeDBConnection(dbConnectionConfig)
    } catch (ex: SQLException) {
        delay(50)
        openDBConnectionWithRetry(dbConnectionConfig)
    }
}

fun dbConnectionConfig(database: JooqGeneratorExtension.Database, dockerUrl: String, dbPort: Int, dbName: String): DBConnectionConfig {
    return when (database) {
        JooqGeneratorExtension.Database.POSTGRES -> DBConnectionConfig("jdbc:postgresql://$dockerUrl:${dbPort}/$dbName", "postgres", "password")
    }
}

data class DBConnectionConfig(val jdbc: String, val user: String, val password: String)