package com.artfable.gradle.jooq.generator.plugin

import org.gradle.api.flow.BuildWorkResult
import org.gradle.api.flow.FlowAction
import org.gradle.api.flow.FlowParameters
import org.gradle.api.logging.Logging
import org.gradle.api.provider.Property
import org.gradle.api.services.ServiceReference
import org.gradle.api.tasks.Input

@Suppress("UnstableApiUsage")
abstract class BuildFinishListener: FlowAction<BuildFinishListener.Params> {
    interface Params: FlowParameters {

        @get:ServiceReference("docker")
        val docker: Property<DockerService>

        @get:Input
        val buildResult: Property<BuildWorkResult>
    }

    override fun execute(parameters: Params) {
        val logger = Logging.getLogger(BuildFinishListener::class.java)
        logger.debug("Build finish, success: ${!parameters.buildResult.get().failure.isPresent}")

        val dockerService = parameters.docker.get()
        if (!dockerService.initialised) {
            logger.debug("Docker service is not initialised, ignoring")
            return
        }

        val containerId = dockerService.dockerContainerId
        val dockerClient = dockerService.dockerClient

        logger.info("Removing container $containerId")
        dockerClient.removeContainerCmd(containerId)?.withForce(true)?.exec()
    }
}