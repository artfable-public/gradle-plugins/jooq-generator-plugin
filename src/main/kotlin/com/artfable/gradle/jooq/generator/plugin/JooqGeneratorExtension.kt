package com.artfable.gradle.jooq.generator.plugin

import org.gradle.api.Incubating
import org.jooq.meta.jaxb.ForcedType

/**
 * @author artfable
 * 14/06/2023
 */
open class JooqGeneratorExtension {
    var migration = "db/changelog.xml"
    var migrationTool = MigrationTool.LIQUIBASE
    var database = Database.POSTGRES
    var imageTag = "latest"
    var language = Language.KOTLIN
    var dockerHost: String? = null
    var dockerURLHost: String = "localhost"
    var dbName = "db"
    var dbDriver = "org.postgresql.Driver"
    var outputPackage = "org.jooq.example"
    var dbPort = 5432
    var schemas = listOf("public")
    var exclude = "Databasechangelog.*|flyway_schema_history"

    @Incubating
    var forcedTypes: List<ForcedType> = emptyList()
        private set

    @Incubating
    fun forcedTypes(block: ForcedTypesDSL.() -> Unit) {
        val typesToBuild: MutableList<ForcedType> = mutableListOf()
        ForcedTypesDSL(typesToBuild).apply(block)
        forcedTypes = typesToBuild
    }

    class ForcedTypesDSL(private val forcedTypes: MutableList<ForcedType>) {
        fun type(block: ForcedType.() -> Unit) {
            forcedTypes.add(ForcedType().apply(block))
        }
    }

    enum class MigrationTool {
        LIQUIBASE, FLYWAY
    }

    enum class Database {
        POSTGRES
    }

    enum class Language {
        KOTLIN, JAVA
    }
}