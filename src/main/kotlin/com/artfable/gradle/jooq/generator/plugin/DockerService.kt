package com.artfable.gradle.jooq.generator.plugin

import com.github.dockerjava.api.DockerClient
import org.gradle.api.services.BuildService
import org.gradle.api.services.BuildServiceParameters

/**
 * The only founded way to share data between tasks when configuration cache is enabled
 */
abstract class DockerService: BuildService<BuildServiceParameters.None> {

    lateinit var dockerContainerId: String
        private set
    lateinit var dockerClient: DockerClient
        private set
    var initialised = false
        private set

    fun init(dockerClient: DockerClient, dbContainerId: String) {
        this.dockerContainerId = dbContainerId
        this.dockerClient = dockerClient
        initialised = true
    }
}