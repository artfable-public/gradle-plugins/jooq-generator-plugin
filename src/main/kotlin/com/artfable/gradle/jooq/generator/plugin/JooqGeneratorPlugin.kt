package com.artfable.gradle.jooq.generator.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.flow.FlowProviders
import org.gradle.api.flow.FlowScope
import org.gradle.api.plugins.JavaBasePlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.kotlin.dsl.*
import javax.inject.Inject


/**
 * @author artfable
 * 14/06/2023
 */
abstract class JooqGeneratorPlugin : Plugin<Project> {

    companion object {
        const val GROUP = "jooq-generator"
    }

    @get:Inject
    @Suppress("UnstableApiUsage")
    protected abstract val flowScope: FlowScope

    @get:Inject
    @Suppress("UnstableApiUsage")
    protected abstract val flowProviders: FlowProviders

    override fun apply(project: Project) {

        val config = project.extensions.create("jooqGenerator", JooqGeneratorExtension::class.java)

        project.plugins.apply(JavaBasePlugin::class)

        val dockerServiceProvider = project.gradle.sharedServices
            .registerIfAbsent("docker", DockerService::class) {}

        val startMigrationDBTask = project.tasks.register<StartMigrationDBTask>("startMigrationDB") {
            group = GROUP
            this.config = config
            dockerInitCallback = { client, containerId ->
                dockerServiceProvider.get().init(client, containerId)
            }
        }.get()

        val migrationTask = project.tasks.register<MigrationTask>("applyMigration") {
            group = GROUP
            this.config = config
            projectDir = project.projectDir.absolutePath
            dependsOn(startMigrationDBTask)
        }.get()

        val jooqTargetDir = "${project.layout.buildDirectory.get()}/generated-src/jooq/main"

        project.extensions.getByType(JavaPluginExtension::class).sourceSets.forEach { src ->
            src.java.srcDir(jooqTargetDir)
        }

        project.tasks.register<JooqGenerationTask>("jooqGeneration") {
            group = GROUP
            this.config = config
            this.jooqTargetDir = jooqTargetDir
            dependsOn(migrationTask)
        }

        @Suppress("UnstableApiUsage")
        flowScope.always(BuildFinishListener::class) {
            parameters.buildResult.set(flowProviders.buildWorkResult)
        }
    }
}