package com.artfable.gradle.jooq.generator.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.*
import org.jooq.meta.jaxb.Target

/**
 * @author artfable
 * 16/06/2023
 */
abstract class JooqGenerationTask: DefaultTask() {

    @get:Input
    lateinit var config: JooqGeneratorExtension

    @get:Input
    lateinit var jooqTargetDir: String

    @TaskAction
    fun process() {
        val connectionConfig = dbConnectionConfig(config.database, config.dockerURLHost, config.dbPort, config.dbName)
        val generator = when (config.language) {
            JooqGeneratorExtension.Language.KOTLIN -> "org.jooq.codegen.KotlinGenerator"
            JooqGeneratorExtension.Language.JAVA -> "org.jooq.codegen.JavaGenerator"
        }

        GenerationTool.generate(
            Configuration()
                .withJdbc(
                    Jdbc()
                        .withDriver(config.dbDriver)
                        .withUrl(connectionConfig.jdbc)
                        .withUsername(connectionConfig.user)
                        .withPassword(connectionConfig.password)
                )
                .withGenerator(
                    Generator()
                        .withName(generator)
                        .withDatabase(
                            Database()
                            .apply {
                                excludes = config.exclude
                                schemata = config.schemas.map { schema -> SchemaMappingType().apply {
                                    inputSchema = schema
                                }}
                                forcedTypes = config.forcedTypes
                            })
                        .withGenerate(
                            Generate().apply {
//                                doesn't seem to be good idea as fields still nullable (make sense in case of joins)
//                                isKotlinNotNullRecordAttributes = true
                                isKotlinNotNullPojoAttributes = true
                                isKotlinNotNullInterfaceAttributes = true
                                isPojosAsKotlinDataClasses = true
                                isPojosAsJavaRecordClasses = true
                            }
                        )
                        .withTarget(
                            Target()
                                .withPackageName(config.outputPackage)
                                .withDirectory(jooqTargetDir)
                        )
                )
        )
    }
}