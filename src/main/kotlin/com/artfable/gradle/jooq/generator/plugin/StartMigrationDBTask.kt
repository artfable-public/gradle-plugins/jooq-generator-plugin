package com.artfable.gradle.jooq.generator.plugin

import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.api.command.PullImageResultCallback
import com.github.dockerjava.api.model.HostConfig
import com.github.dockerjava.api.model.PortBinding
import com.github.dockerjava.core.DefaultDockerClientConfig
import com.github.dockerjava.core.DockerClientImpl
import com.github.dockerjava.zerodep.ZerodepDockerHttpClient
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

/**
 * @author artfable
 * 15/06/2023
 */
abstract class StartMigrationDBTask: DefaultTask() {

    @get:Input
    lateinit var dockerInitCallback: (DockerClient, dbContainerId: String) -> Unit

    @get:Input
    lateinit var config: JooqGeneratorExtension

    @TaskAction
    fun process() {
        val dockerConf = DefaultDockerClientConfig.createDefaultConfigBuilder()
            .apply {
                config.dockerHost?.let(this::withDockerHost)
            }.build()
        val dockerClient = DockerClientImpl.getInstance(
            dockerConf,
            ZerodepDockerHttpClient.Builder()
                .dockerHost(dockerConf.dockerHost)
                .sslConfig(dockerConf.sslConfig)
                .build()
        )

        if (dockerClient.listImagesCmd().withReferenceFilter("postgres:${config.imageTag}").exec().isEmpty()) {
            logger.info(
                dockerClient.pullImageCmd("postgres:${config.imageTag}").exec(PullImageResultCallback()).awaitCompletion().toString()
            )
        }
        val containerResponse = dockerClient.createContainerCmd("postgres:${config.imageTag}")
            .withEnv("POSTGRES_PASSWORD=password", "POSTGRES_USER=postgres", "POSTGRES_DB=${config.dbName}")
            .withHostConfig(HostConfig.newHostConfig().withPortBindings(PortBinding.parse("${config.dbPort}:5432")))
            .exec()
        dockerClient.startContainerCmd(containerResponse.id).exec()
        dockerInitCallback(dockerClient, containerResponse.id)
    }
}