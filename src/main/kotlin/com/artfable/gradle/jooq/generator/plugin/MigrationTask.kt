package com.artfable.gradle.jooq.generator.plugin

import kotlinx.coroutines.runBlocking
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import java.io.File

/**
 * @author artfable
 * 15/06/2023
 */
abstract class MigrationTask: DefaultTask() {

    private val timeout = 10000L

    @get:Input
    lateinit var config: JooqGeneratorExtension

    @get:Input
    lateinit var projectDir: String

    @TaskAction
    fun process() {
        logger.info("Loading class: " + Class.forName(config.dbDriver).name)

        val dbConfig = dbConnectionConfig(config.database, config.dockerURLHost, config.dbPort, config.dbName)
        val migration = migration(config.migrationTool, config.migration, File(projectDir), dbConfig)
        runBlocking {
            migration.run(dbConfig, timeout)
        }
    }
}