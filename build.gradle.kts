import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    `kotlin-dsl`
    `maven-publish`
}

group = "com.artfable.gradle"
version = "0.2.3"

val gitlabToken = findProperty("gitlabPersonalApiToken") as String?

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.1")
    implementation("com.github.docker-java:docker-java:3.4.1")
    implementation("com.github.docker-java:docker-java-transport:3.4.1")
    implementation("com.github.docker-java:docker-java-transport-zerodep:3.4.1")
    implementation("org.testcontainers:postgresql:1.19.8")
    implementation("org.liquibase:liquibase-core:4.28.0")
    implementation("org.flywaydb:flyway-core:10.14.0")
    implementation("org.flywaydb:flyway-database-postgresql:10.14.0")
    implementation("org.jooq:jooq-codegen:3.19.9")
    implementation("org.postgresql:postgresql:42.7.3")
}

gradlePlugin {
    website.set("https://gitlab.com/artfable-public/gradle-plugins/jooq-generator-plugin")
    vcsUrl.set("https://gitlab.com/artfable-public/gradle-plugins/jooq-generator-plugin")
    plugins {
        create("jooqGeneratorPlugin") {
            id = "artfable.jooq-generator"
            implementationClass = "com.artfable.gradle.jooq.generator.plugin.JooqGeneratorPlugin"
            displayName = "JOOQ Generator Plugin"
            description = "JOOQ generation from migration applied on DB with testcontainers"
            tags = listOf("jooq")
        }
    }
}

kotlin {
    compilerOptions {
        jvmTarget = JvmTarget.JVM_1_8
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
    withJavadocJar()
}

publishing {
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/46894628/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitlabToken
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}
