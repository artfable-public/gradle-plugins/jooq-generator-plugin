# Gradle JOOQ Generator Plugin

Note! there's breaking changes with JOOQ, code generated with the plugin below version 0.2.0 will not compile with JOOQ 3.19+ 

## Overview
Plugin that generating jooq classes for DB.
Has 3 tasks:
- startMigrationDB - start testcontainer with the DB
- applyMigration - apply migration to testcontainer DB with selected migration tool
- jooqGeneration - generate JOOQ code from testcontainer DB

Automatically shut down testcontainer DB after build finished.

## Install
Note! Docker should be running

in _settings.gradle.kts_:
```kotlin
pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        maven(url = "https://gitlab.com/api/v4/groups/68820060/-/packages/maven")
    }
}
```

in build script:
```kotlin
plugins {
    id("artfable.jooq-generator") version "0.2.3"
}
```

## Usage
```kotlin
jooqGenerator {
    migration = "path to changelog.xml for liquibase or flyway directory with SQL" // default: ""db/changelog.xml"
    migrationTool = MigrationTool.LIQUIBASE // default
    database = Database.POSTGRES // default
    imageTag = "latest" // default
    language = Language.KOTLIN // default, supported JAVA, KOTLIN
    dockerHost = null // override if need custom, default for unix: unix:///var/run/docker.sock
    dockerURLHost = "example.com" // default: localhost
    dbPort = 5432 // default. port to witch container will be exposed
    dbName = "db" // default: db 
    dbDriver = "org.postgresql.Driver" // default
    outputPackage = "org.jooq.example" // default
    schemas = listOf("public") // default; list of schemas to generate from
    exclude = "Databasechangelog.*|flyway_schema_history" // default; pattern to exclude from generation of jooq classes
    
    forcedTypes { // feature is @Incubating, can be changed in minor versions
        type { // org.jooq.meta.jaxb.ForcedType
            withUserType(Instant::class.qualifiedName)
            withConverter("org.example.JooqInstantConverter")
            withIncludeTypes("timestamp")
        }
        type {  }
    }
}
```

For more [org.jooq.meta.jaxb.ForcedType](https://www.jooq.org/doc/latest/manual/code-generation/codegen-advanced/codegen-config-database/codegen-database-forced-types/codegen-database-forced-types-matching/)

## Versions compatibility
Generated code for JOOQ can have breaking changes between the versions. 
You can use alternative version of code generation lib (see example below) but there's no guaranty for this.
```kotlin
buildscript {
    dependencies {
        classpath("org.jooq:jooq-codegen:3.19.9")
    }
}
```
### Version used

| Plugin version | JOOQ version |
|----------------|--------------|
| 0.2.0 - 0.2.3  | 3.19.9       |
| 0.1.3 - 0.1.6  | 3.18.7       |
| 0.1.0 - 0.1.2  | 3.18.4       |
